const { v4: uuidv4 } = require('uuid');

class TransactionsManager {

    constructor() {
        this.transactions = [];
        // Initialize balance with a random number between 0 and 99;
        this.balance = Math.floor(Math.random() * 100);
    }
    
    ProcessTransaction(transaction) {
        return new Promise((resolve, reject) => {
            transaction = this.SetTransactionId(transaction);
            let transactionValue = Number(transaction.amount);
            switch (transaction.type) {
                case "debit":
                    if (this.balance - transactionValue < 0) {
                        console.log("transaction failed")
                        transaction.status = "Declined";
                    } else {
                        this.balance = this.balance - transactionValue
                        transaction.status = "Completed";
                    }
                    break;
                case "credit":
                    this.balance = this.balance + transactionValue
                    transaction.status = "Completed";
                    break;
                default:
                    transaction.status = "Declined";
                    break
            }
            this.transactions.push(transaction);
            return resolve(transaction);
        });
    }

    SetTransactionId(transaction) {
        transaction.id = uuidv4();
        return transaction;
    }

    FindTransaction(id) {
        return this.transactions.filter((transaction) =>  transaction.id === id);
    }
}

class Singleton {

    constructor() {
        if (!Singleton.instance) {
            Singleton.instance = new TransactionsManager();
        }
    }

    getInstance() {
        return Singleton.instance;
    }

}

module.exports = Singleton;