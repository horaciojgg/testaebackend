# Api

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run start
```



### With Docker
```
docker build api

docker run -p 8080:3000 -d api

Url: localhost:3000
```
