const express = require('express');
const app = express();
const port = 3000;
const transactionsRoutes = require('./routes/transactions');
const balanceRoutes = require('./routes/balance');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use('/transactions', transactionsRoutes);
app.use('/balance', balanceRoutes);


app.get('/', (req, res) => {
  res.send('');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});

