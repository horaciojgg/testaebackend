const express = require('express');
const router = express.Router();
var Singleton = require("../queue/TransactionManager");
let instance = new Singleton().getInstance();

router.get("/", async (req, res)=>{
    res.status(200).send(instance.balance.toString());
});


module.exports = router;