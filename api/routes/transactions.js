const express = require('express');
const router = express.Router();
var Singleton = require("../queue/TransactionManager");
let instance = new Singleton().getInstance();

router.get("/", async (req, res)=>{
    res.send(instance.transactions);
});

router.get("/:id", async (req, res)=>{
    let transaction = instance.FindTransaction(req.params.id);
    res.send(transaction);
});

router.post("/", (req, res) => {
    instance.ProcessTransaction(req.body).then(transaction => {
        if (transaction.status === 'Completed') {
            res.sendStatus(200);
        } else  {
            res.sendStatus(403);
        }
    }, reject => {
        res.sendStatus(500);
    })
});

module.exports = router;